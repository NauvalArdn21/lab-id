<?php

namespace App\Http\Controllers;

use App\Page;
use App\Slide;

class AppController extends Controller {
    public function index() {
        $images = Page::with('images')->where('title', 'LIKE', '%home%')->first()->images;
        $logo = Page::with('images')->where('title', 'logo_news')->first();
        $inq = Page::with('images')->where('title', 'logo_inq')->first();
        $media = Page::with('images')->where('title', 'logo_media')->first();
        $about = Page::with('images')->where('title', 'logo_about')->first();
        $slideOne = Slide::with('images')->where('slider_id', 1)->get();
        $slideTwo = Slide::with('images')->where('slider_id', 2)->get();
        $slideThree = Slide::with('images')->where('slider_id', 3)->get();
        $slideFour = Slide::with('images')->where('slider_id', 4)->get();
        $randomSlide = collect([
            'main.partials.slide_1',
            'main.partials.slide_2',
            'main.partials.slide_3',
            'main.partials.slide_4',
        ]);
        $projects = Page::oldest()->take(75)->get();
        // foreach (Slide::where('name', 'LIKE', '%' . 'project' . '%')->get() as $slide) {
        //     if (count($slide->images) === 0) {
        //         abort(500, 'Image Not Set');
        //     }
        // }

        return view('pages.home.index', compact(
            'images',
            'slideOne',
            'slideTwo',
            'slideThree',
            'randomSlide',
            'projects',
            'slideFour',
            'logo',
            'inq',
            'about',
            'media'
        ));
    }

    public function about() {
        $slideOne = Page::with('images', 'pageDescriptions')->where('title', 'about-lab')->first();
        $slideTwo = Page::with('images', 'pageDescriptions')->where('title', 'partner')->first();
        $slideThree = Page::with('images', 'pageDescriptions')->where('title', 'lab-people')->first();
        $slideFour = Page::with('images', 'pageDescriptions')->where('title', 'client')->first();

        return view('pages.about.index', compact('slideOne', 'slideTwo', 'slideThree', 'slideFour'));
    }

    public function inquiries() {
        $page = Page::with('pageDescriptions')->where('title', 'LIKE', '%inquiries%')->first();

        return view('pages.inquiries.index', compact('page'));
    }

    public function news() {
        $page = Page::with('pageDescriptions')->where('title', 'LIKE', '%news%')->first();
        $slideFour = Page::with('images', 'pageDescriptions')->where('title', 'News Update')->first();
        return view('pages.news.index', compact('page','slideFour'));
    }

    public function detailProject(Page $page) {
        $page->load('projectDetails');
        $orderedImages = $page->images->sortBy('pivot.order');
        // dd($orderedImages);
        return view('pages.project.show', compact('page', 'orderedImages'));
    }
}
