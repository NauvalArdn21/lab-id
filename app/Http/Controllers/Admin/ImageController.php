<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InvImage;
use Illuminate\Support\Str;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $thumbs = Image::where('src', 'LIKE', '%_thumb%')->get();
        $images = Image::where('src', 'NOT LIKE', '%_thumb%')->get();

        return view('pages.admin.image.index', compact('images', 'thumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ini_set('memory_limit', '512M');

        $request->validate(['image' => 'required|image|max:2048']);

        $basename = Str::random();
        $originalPath = 'uploads/images/' . $basename . '.' . $request->image->getClientOriginalExtension();
        $thumbnailPath =  'uploads/images/' . $basename . '_thumb.' . $request->image->getClientOriginalExtension();

        $thumbnail = InvImage::make($request->image);

        Image::create(['src' => $thumbnailPath]);

        if ($request->image->getClientOriginalExtension() == 'gif') {
            copy($request->image->getRealPath(), storage_path('app/public/' . $thumbnailPath));
        } else {
            if ($thumbnail->filesize() >= 1024000) {
                $thumbnail->resize(null, 800, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save(storage_path('app/public/' . $thumbnailPath));
            } else {
                $thumbnail->resize(null, 1000, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save(storage_path('app/public/' . $thumbnailPath));
            }
        }

        Image::create(['src' => $originalPath]);

        $request->file('image')->move(storage_path('app/public/uploads/images'), $originalPath);

        session()->flash('message', 'Image successfully uploaded!');
        session()->flash('alert-class', 'alert-success');

        return redirect(route('images.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        $thumbnailImgFile = explode('.', $image->src)[0] . '_thumb.' . explode('.', $image->src)[1];
        $thumbnailImg = Image::where('src', $thumbnailImgFile)->first();

        Storage::delete('/storage/' . $image->src);
        Storage::delete('/storage/' . $thumbnailImgFile);

        $image->delete();
        $thumbnailImg->delete();

        session()->flash('message', 'Image successfully deleted!');
        session()->flash('alert-class', 'alert-danger');

        return redirect(route('images.index'));
    }
}
