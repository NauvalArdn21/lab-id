<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Image;
use App\Page;
use App\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class PageController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $page = Page::where('title', 'LIKE', '%home%')->first();
        $images = Image::where('src', 'LIKE', '%_thumb%')->get();
        $slideOne = Slide::with('images')->where('slider_id', 1)->get();
        $slideTwo = Slide::with('images')->where('slider_id', 2)->get();
        $slideThree = Slide::with('images')->where('slider_id', 3)->get();
        $slideFour = Slide::with('images')->where('slider_id', 4)->get();

        return view('pages.admin.page.index', compact('page', 'images', 'slideOne', 'slideTwo', 'slideThree','slideFour'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = $request->validate([
            'left_description' => 'required',
            'right_description' => 'required'
        ]);
        Page::create($rules);
        return redirect(route('pages.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page) {
        // dd("oke");
        $page->load('pageDescriptions', 'images');
        // dd($page->load('pageDescriptions', 'images'));
        // $order = [];
        $thumbs = [];
        foreach ($page->images->sortBy('pivot.order') as $image) {
            // $idOfImages[] = $image->id - 1;
            $temp = DB::table('imageables')->where('image_id', $image->id)->first();
            // $order[] = $temp->order; 
            $temp2 = Image::where('id', $image->id - 1)->first();
            $temp2['order'] = $temp->order;
            $thumbs[] = $temp2;
            // $order[] = $temp->order;
            // $idOfImages[] = $image;
        }

        // dd($thumbs);

        
        // $thumbs = Image::where('src', 'LIKE', '%_thumb%')
        // ->whereIn('id', $idOfImages)
        // ->get();
        // dd($thumbs);
        // dd($page->pageDescriptions[0]->id);

        return view('pages.admin.page.show', compact('page', 'thumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page) {
        return view('page.edit', compact('page'));
    }

    public function update(Page $page) {
        $data = request()->validate(['name' => 'required']);

        $page->update(array_merge($data, ['slug' => Str::slug($data['name'])]));

        session()->flash('message', $page->title . ' Page successfully updated!');
        session()->flash('alert-class', 'alert-info');

        return redirect(route('pages.show', $page->id));
    }


    public function updateImage(Page $page) {
        $page->images()->sync(request('images'));

        session()->flash('message', $page->title . ' Slider Image successfully updated!');
        session()->flash('alert-class', 'alert-info');

        if ($page->title == 'about-lab' || $page->title == 'partner' || $page->title == 'lab-people' || $page->title == 'client') {
            return redirect(route('pages.about'));
        } else {
            return redirect(route('pages.show', $page->id));
        }
    }

    public function updateImageOrder(Request $request ,Page $page) {
       $project = $page->load('pageDescriptions', 'images');
    //    dd($project->id);
        $request->validate([
            'order' => 'required',
            'order.*' => 'required',
        ]);
        $order = $request->order;
        $image = $request->images;
        for($i = 0; $i < count($request->order);$i++){
                DB::table('imageables')
                    ->where('image_id', $image[$i])
                    ->where('imageable_id', $project->id)
                    ->update(['order' => $order[$i]]);
        }
        // foreach($request->image as $img){
        // }
        // dd($request->order, $request->images);
        // $extra = array_map(function ($order) {
        //     return ['order' => $order];
        // }, request('order'));

        // $data = array_combine(request('images'), $extra);

        // $page->images()->syncWithoutDetaching($data);

        session()->flash('message', $page->title . ' Slider Image Order successfully updated!');
        session()->flash('alert-class', 'alert-info');

        return redirect(route('pages.show', $page->id));
    }

    public function deleteImage() {
        $page = Page::where('title', 'LIKE', '%about%')->first();

        $page->images()->detach(request('images'));
        session()->flash('alert-class', 'alert-danger');

        return redirect(route('pages.about'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page) {
        $page->delete();
    }

    public function about() {
        $slideOne = Page::with('images', 'pageDescriptions')->where('title', 'about-lab')->first();
        $logo = Page::with('images')->where('title', 'logo_about')->first();
        $slideTwo = Page::with('images', 'pageDescriptions')->where('title', 'partner')->first();
        $slideThree = Page::with('images', 'pageDescriptions')->where('title', 'lab-people')->first();
        $slideFour = Page::with('images', 'pageDescriptions')->where('title', 'client')->first();

        return view('pages.admin.page.show_about', compact('slideOne', 'slideTwo', 'slideThree', 'slideFour','logo'));
    }

    public function chooseImage(Page $page) {
        $page->load('images');

        $thumbs = Image::where('src', 'LIKE', '%_thumb%')->get();
        $images = Image::where('src', 'NOT LIKE', '%_thumb%')->get();

        return view('page.choose_image', compact('images', 'page', 'thumbs'));
    }

    public function news() {
        $slideOne = Page::with('images')->where('title', 'logo_news')->first();
        $page = Page::with('images', 'pageDescriptions')->where('title', 'News Update')->first();
        
        return view('pages.admin.page.show_news', compact('page','slideOne'));
    }
    
    public function inquiries() {
        $slideOne = Page::with('images', 'pageDescriptions')->where('title', 'logo_inq')->first();
        $page = Page::with('images', 'pageDescriptions')->where('title', 'inquiries')->first();

        return view('pages.admin.page.show_main', compact('page','slideOne'));
    }
    public function media() {
        $media = Page::with('images','pageDescriptions')->where('title', 'logo_media')->first();
        return view('pages.admin.page.show_media', compact('media'));
    }
}
