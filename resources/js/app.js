require("./bootstrap");

document.body.addEventListener("keydown", function(event) {
  if (event.key === "Escape") {
    document.location.href = "/";
  }
});

// initial state document
const slideOne = document.querySelectorAll(".slide-1");
const slideTwo = document.querySelectorAll(".slide-2");
const slideThree = document.querySelectorAll(".slide-3");

let slideActive = document.querySelector("section.swiper-slide-active");
let bigProjectActive = document.querySelector(
  "section.swiper-slide-active .col-span-2"
);

function randomPosition() {
  const slideActiveId = slideActive.getAttribute("id");

  const shuffled = Array.from(slideActive.children)
    .map((a) => ({
      sort: Math.random(),
      value: a,
    }))
    .sort((a, b) => a.sort - b.sort)
    .map((a) => a.value);

  if (slideActiveId === "slide-1") {
    for (let i = 4; i < shuffled.length; i++) {
      const element = shuffled[i];

      if (element === bigProjectActive) {
        randomPosition();

        return;
      }
    }
  } else if (slideActiveId === "slide-2") {
    for (let i = 9; i < shuffled.length; i++) {
      const element = shuffled[i];

      if (element === bigProjectActive) {
        randomPosition();

        return;
      }
    }
    for (let i = 0; i < 5; i++) {
      const element = shuffled[i];

      if (element === bigProjectActive) {
        randomPosition();

        return;
      }
    }
  } else {
    for (let i = 4; i < shuffled.length; i++) {
      const element = shuffled[i];

      if (element === bigProjectActive) {
        randomPosition();

        return;
      }
    }
  }

  shuffled.forEach((el) => {
    slideActive.appendChild(el);
  });

  slideActive = document.querySelector("section.swiper-slide-active");
  bigProjectActive = document.querySelector(
    "section.swiper-slide-active .col-span-2"
  );
}

// random all slide action
function randomSliderImage() {
  randomPosition();
}

// start random action
function startRandomSlider() {
  randomSliderImageInterval = setInterval(randomSliderImage, randomDuration);
}

// stop random action
function stopRandomSlider() {
  clearInterval(randomSliderImageInterval);
}

// initial random when load web
let randomSliderImageInterval = setInterval(randomSliderImage, randomDuration);

// stop random action when click project at slide one
slideOne.forEach((slide) => {
  for (child of slide.children) {
    child.addEventListener("mouseenter", () => {
      stopRandomSlider();
    });
    child.addEventListener("mouseleave", () => {
      startRandomSlider();
    });
  }
});

// stop random action when click project at slide two
slideTwo.forEach((slide) => {
  for (child of slide.children) {
    child.addEventListener("mouseenter", () => {
      stopRandomSlider();
    });
    child.addEventListener("mouseleave", () => {
      startRandomSlider();
    });
  }
});

// stop random action when click project at slide three
slideThree.forEach((slide) => {
  for (child of slide.children) {
    child.addEventListener("mouseenter", () => {
      stopRandomSlider();
    });
    child.addEventListener("mouseleave", () => {
      startRandomSlider();
    });
  }
});
