@extends('layouts.guest', ['title' => 'About', 'description' => $slideOne->pageDescriptions[0]->description])

@section('content')
<div class="app min-h-screen flex flex-col overflow-x-hidden">
    <div class="flex-1 w-full h-full swiper-container xxl:mt-20">
        <div class="swiper-wrapper">
            <section class="swiper-slide">
                <header class="mt-5">
                    <h1 class="text-lg font-bold tracking-widest uppercase">About LAB</h1>
                </header>

                <div class="flex flex-wrap mt-12">
                    @foreach ($slideOne->images as $image)
                    <div class="w-full lg:w-1/3 self-start relative mb-8 lg:mr-8 lg:mr-0" >
                        <img src="{{ asset('storage/'.$image->src) }}" class="w-full">
                        {{-- <div class="nested-swiper-container overflow-hidden w-full">
                            <div class="swiper-wrapper">
                                @foreach ($slideOne->images as $image)
                                <div class="swiper-slide">
                                    <img src="{{ asset('storage/'.$image->src) }}" class="w-full">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="z-20 btn-next-nested absolute top-0 right-0 bottom-0 left-0 outline-none"></div>
                        --}}
                    </div>
                    @endforeach

                    <div class="w-full mt-10 lg:-mt-1 lg:flex-1 lg:pl-20 lg:ml-4">
                        <p class="text-xs xxl:text-base whitespace-pre-line">{{
                            $slideOne->pageDescriptions[0]->description }}</p>
                    </div>
                </div>

                <div class="mt-10 lg:mt-10 lg:hidden">
                    <a href="{{ route('/') }}">
                        <img class="w-7" src="{{ asset('img/BACK.png') }}" alt="back btn">
                    </a>
                </div>
            </section>
            <section class="swiper-slide">
                <header class="mt-5">
                    <h1 class="text-lg font-bold tracking-widest uppercase">Partner</h1>
                </header>

                <div class="flex flex-wrap mt-12">
                    <div class="w-full lg:w-1/3 self-start relative">
                        @foreach ($slideTwo->images as $image)
                        <img src="{{ asset('storage/'.$image->src) }}" class="w-full">
                        @endforeach
                        {{-- <div class="nested-swiper-container overflow-hidden w-full">
                            <div class="swiper-wrapper">
                                @foreach ($slideTwo->images as $image)
                                <div class="swiper-slide">
                                    <img src="{{ asset('storage/'.$image->src) }}" class="w-full">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="z-20 btn-next-nested absolute top-0 right-0 bottom-0 left-0 outline-none"></div>
                        --}}
                    </div>
                    <div class="w-full mt-10 lg:-mt-1 lg:flex-1 lg:pl-20 lg:ml-4">
                        <p class="text-xs xxl:text-base whitespace-pre-line">{{
                            $slideTwo->pageDescriptions[0]->description }}</p>
                    </div>
                </div>

                <div class="mt-10 lg:mt-10 lg:hidden">
                    <a href="{{ route('/') }}">
                        <img class="w-7" src="{{ asset('img/BACK.png') }}" alt="back btn">
                    </a>
                </div>
            </section>
            <section class="swiper-slide">
                <header class="mt-5">
                    <h1 class="text-lg font-bold tracking-widest uppercase">LAB People</h1>
                </header>

                <div class="flex flex-wrap mt-12">
                    <div class="w-full lg:w-1/3 self-start relative">
                        @foreach ($slideThree->images as $image)
                        <img src="{{ asset('storage/'.$image->src) }}" class="w-full">
                        @endforeach
                        {{-- <div class="nested-swiper-container overflow-hidden">
                            <div class="swiper-wrapper">
                                @foreach ($slideThree->images as $image)
                                <div class="swiper-slide">
                                    <img src="{{ asset('storage/'.$image->src) }}" class="w-full">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="z-20 btn-next-nested absolute top-0 right-0 bottom-0 left-0 outline-none"></div>
                        --}}
                    </div>
                    
       
            <div class="w-full lg:flex-1 flex flex-col md:flex-row lg:mt-12 lg:pl-20 lg:ml-4 text-xs xxl:text-base" style=" height: 380px; overflow-y: scroll; position: relative;" >
            <p class="w-full lg:w-1/3 whitespace-pre-line">{{ $slideThree->pageDescriptions[0]->description }}</p>
            <p class="w-full lg:w-1/3 whitespace-pre-line lg:mt-xxl:mt-15" style="margin-top: 3em;margin-left:50px">{{ $slideThree->pageDescriptions[1]->description }}</p>
            <p class="w-full lg:w-1/3 whitespace-pre-line lg:mt-xxl:mt-15" style="margin-top: 3em;margin-left:50px">{{ $slideThree->pageDescriptions[2]->description }}</p>
        </div>
    </div>



                <div class="mt-10 lg:mt-10 lg:hidden">
                    <a href="{{ route('/') }}">
                        <img class="w-7" src="{{ asset('img/BACK.png') }}" alt="back btn">
                    </a>
                </div>
            </section>
            <section class="swiper-slide">
                <header class="mt-5">
                    <h1 class="text-lg font-bold tracking-widest uppercase">Client</h1>
                </header>

                <div class="flex flex-wrap mt-12">
                    <div class="w-full lg:w-1/3 self-start relative">
                        @foreach ($slideFour->images as $image)
                        <img src="{{ asset('storage/'.$image->src) }}" class="w-full">
                        @endforeach
                        {{-- <div class="nested-swiper-container overflow-hidden">
                            <div class="swiper-wrapper">
                                @foreach ($slideFour->images as $image)
                                <div class="swiper-slide">
                                    <img src="{{ asset('storage/'.$image->src) }}" class="w-full">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="z-20 btn-next-nested absolute top-0 right-0 bottom-0 left-0 outline-none"></div>
                        --}}
                    </div>
                    <div
                        class="w-full lg:flex-1 flex flex-col md:flex-row mt-10 lg:-mt-1 lg:pl-20 lg:ml-4 text-xs xxl:text-base">
                        <p class="w-1/3 whitespace-pre-line">{{ $slideFour->pageDescriptions[0]->description }}</p>
                        <p class="w-1/3 whitespace-pre-line lg:mt-9 xxl:mt-14">{{
                            $slideFour->pageDescriptions[1]->description }}</p>
                    </div>

                    <div class="mt-10 lg:mt-10 lg:hidden">
                        <a href="{{ route('/') }}">
                            <img class="w-7" src="{{ asset('img/BACK.png') }}" alt="back btn">
                        </a>
                    </div>
            </section>
        </div>

        <div class="btn-prev fixed outline-none">
            <svg class="w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175">
                <path d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225
                    c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z" />
            </svg>
        </div>

        <div class="btn-next fixed outline-none">
            <svg class="w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175">
                <path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5
                    c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z
                " />
            </svg>
        </div>
    </div>

    <footer class="hidden lg:block">
        <a href="{{ route('/') }}">
            <img class="w-7" src="{{ asset('img/BACK.png') }}" alt="back btn">
        </a>
    </footer>
</div>
@endsection
@push('js')
<script>
    window.addEventListener('DOMContentLoaded', () => {
        const aboutSwiper = new Swiper('.swiper-container', {
            loop: true,
            spaceBetween: 30,
            speed: 750,
            nested: true,
            simulateTouch: false,
            navigation: {
                nextEl: '.btn-next',
                prevEl: '.btn-prev',
            },
            keyboard: {
                enabled: true,
                onlyInViewport: false,
            }
        });

        document.addEventListener('keyup', function(event) {
            if (event.key === 'Escape') {
                window.location.href = '/';
            }
        });
    });
</script>

<style>
  /* Ganti warna latar belakang, lebar, dan tinggi scrollbar sesuai kebutuhan Anda */
  ::-webkit-scrollbar {
    width: 8px;
    height: 8px;
    background-color: #f5f5f5;
  }

  /* Ganti warna latar belakang bagian luar scrollbar sesuai kebutuhan Anda */
  ::-webkit-scrollbar-track {
    background-color: #f5f5f5;
  }

  /* Ganti warna latar belakang bagian dalam scrollbar sesuai kebutuhan Anda */
  ::-webkit-scrollbar-thumb {
    background-color: #888;
    border-radius: 3px;
  }
</style>

@endpush
