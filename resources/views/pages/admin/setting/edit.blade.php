@extends('layouts.app', ['title' => 'Site Setting'])

@section('content')
@include('layouts.headers.cards')

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <h3 class="col-12 mb-0">Edit Site Setting</h3>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('settings.update', $setting) }}" autocomplete="off"
                        enctype="multipart/form-data">
                        @csrf
                        @method('put')

                        @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        <div class="pl-lg-4">
                            <div class="form-group{{ $errors->has('site_name') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="site-name">Site Name</label>
                                <input type="text" name="site_name" id="site-name"
                                    class="form-control form-control-alternative{{ $errors->has('site_name') ? ' is-invalid' : '' }}"
                                    placeholder="Site name" value="{{ old('site_name', $setting->site_name) }}"
                                    autofocus>

                                @error('site_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group{{ $errors->has('site_description') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="site-description">Site Description</label>
                                <textarea
                                    class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                    placeholder="Site description" name="site_description" id="site-description"
                                    rows="5">{{ old('site_description', $setting->site_description) }}</textarea>
                                @if ($errors->has('site_description'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('site_description') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="site-logo">Site Logo</label>
                                <img class="d-block w-25" src="{{ asset($setting->site_logo) }}">
                            </div>
                            <div class="form-group{{ $errors->has('site_media_url') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="site-name">Site Media URL</label>
                                <input type="text" name="site_media_url" id="site-name"
                                    class="form-control form-control-alternative{{ $errors->has('site_media_url') ? ' is-invalid' : '' }}"
                                    placeholder="Site name"
                                    value="{{ old('site_media_url', $setting->site_media_url) }}" autofocus>

                                @error('site_media_url')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            {{-- <div class="form-group{{ $errors->has('site_logo') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="site-logo">Site Logo (Upload for Replace)</label>
                            <input type="file" name="site_logo" id="site-logo"
                                class="form-control form-control-alternative{{ $errors->has('site_logo') ? ' is-invalid' : '' }}"
                                autofocus>
                            @if ($errors->has('site_logo'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('site_logo') }}</strong>
                            </span>
                            @endif
                        </div> --}}

                        <div class="text-center">
                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('layouts.footers.auth')
</div>
@endsection
