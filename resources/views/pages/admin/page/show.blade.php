@extends('layouts.app', ['title' => 'Project Details'])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0 d-flex justify-content-between align-items-center">
                        <h3 class="mb-0">Project Details | {{ $page->title }}</h3>
                    </div>
                    <hr class="my-1">
                    <div class="card-body">
                        @if (request()->session()->has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                                <span class="alert-inner--text">
                                    {{ session()->get('message') }}
                                </span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span class="alert-inner--icon"><i class="ni ni-dislike-2"></i></span>
                                <span class="alert-inner--text">
                                    "Semua Gambar Wajib Memiliki Angka Urutan!"
                                </span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <div class="d-flex flex-column border mb-3 rounded card-body">
                            <div class="d-flex mb-4 justify-content-between">
                                <div class="d-flex">
                                    <h4 class="mr-3 mb-0">Slider Images</h4>
                                    <a href="{{ route('pages.choose_image', $page->id) }}" class="btn btn-sm btn-primary">
                                        Choose Image
                                    </a>
                                </div>
                                <div>a
                                </div>
                            </div>

                            <hr class="m-0 border">

                            <form action="{{ route('pages.update_image_order', $page->id )}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="grid-project mt-3">
                                    @forelse ($thumbs as $index => $image)
                                        <div class="mr-2 mb-2 flex flex-column justify-content-center">
                                            <img loading="lazy" src="{{ asset('storage/'.$image->src) }}" class="w-100 h-150px rounded">
                                            <div class="w-50 mt-2">
                                                <input type="text" name="order[]" class="form-control" value="{{ $image->order ?? '' }}">
                                                <input type="hidden" name="images[]" value="{{ $image->id + 1 }}">
                                            </div>
                                        </div>
                                    @empty
                                        <div class="text-red text-sm font-weight-bold">Slider image is empty!</div>
                                    @endforelse
                                </div>
                                @if (count($page->images) > 0)
                                    <button class="w-100 mt-2 btn btn-sm btn-primary">Change Order</button>
                                @endif
                            </form>
                        </div>

                        <div class="border card-body">
                            <form action="{{ route('pages.update', $page) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="description">Project Name</label>
                                    <input name="name" id="description" value="{{ $page->name }}" class="form-control">
                                </div>
                                <button type="submit" class="btn btn-success">Save</button>
                            </form>

                            @if (isset($page->pageDescriptions[0]))
                                <form action="{{ route('pageDescriptions.update', [$page->id, $page->pageDescriptions[0]->id ]) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group mt-4">
                                        <label for="description">Left Description</label>
                                        <textarea name="description[description]" id="description" class="form-control" cols="30" rows="15">{{ $page->pageDescriptions[0]->description ?? '' }}</textarea>
                                    </div>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </form>
                            @endif

                            @if (isset($page->pageDescriptions[1]))
                                <form action="{{ route('pageDescriptions.update', [$page->id, $page->pageDescriptions[1]->id ]) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group mt-4">
                                        <label for="description">Right Description</label>
                                        <textarea name="description[description]" id="description" class="form-control" cols="30" rows="15">{{ $page->pageDescriptions[1]->description ?? '' }}</textarea>
                                    </div>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </form>
                            @endif

                            @if (isset($page->pageDescriptions[2]))
                                <form action="{{ route('pageDescriptions.update', [$page->id, $page->pageDescriptions[2]->id ]) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group mt-4">
                                        <label for="description">Right Description</label>
                                        <textarea name="description[description]" id="description" class="form-control" cols="30" rows="15">{{ $page->pageDescriptions[2]->description ?? '' }}</textarea>
                                    </div>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    @include('layouts.footers.auth')
@endsection
