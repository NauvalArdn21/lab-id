@extends('layouts.app', ['title' => 'Main Page'])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0 d-flex justify-content-between align-items-center">
                        <h3 class="mb-0">Media</h3>
                    </div>
                    <hr class="my-1">
                    <div class="card-body">
                        @if (request()->session()->has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                                <span class="alert-inner--text">
                                    {{ session()->get('message') }}
                                </span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card-body border">
                            <h4 class="mr-3 mb-0"> Logo Content</h4>
                        </div>
                        <div class="card-body border">
                                <div class="d-flex flex-column border mb-3 rounded card-body">
                                    <div class="d-flex mb-4">
                                        <h4 class="mr-3 mb-0">Slider Images</h4>
                                        @if (isset($media) && is_object($media) && isset($media->id))
                            <a href="{{ route('pages.choose_image', $media->id) }}" class="btn btn-sm btn-primary">
                                Choose image
                            </a>
                        @else
                            <span class="text-red">Slide tidak tersedia</span>
                        @endif
                    </div>

                            <hr class="m-0 border">

                            <div class="d-flex flex-wrap mt-3">
                            @foreach ($media->images as $image)
                                <div class="w-25 mr-2 mb-2">
                                    <img loading="lazy" src="{{ asset('storage/'.$image->src) }}" class="w-100 h-150px rounded">
                                </div>
                            @endforeach
                         4v             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    @include('layouts.footers.auth')
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
