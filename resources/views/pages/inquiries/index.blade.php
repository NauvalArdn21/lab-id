@extends('layouts.guest', ['title' => 'Contact', 'description' => $page->pageDescriptions[0]->description])

@section('content')
    <div class="app min-h-screen flex flex-col overflow-x-hidden">
        <div class="flex-1 w-full h-full swiper-container xxl:mt-20">
            <div class="swiper-wrapper">
                <section class="swiper-slide">
                    <header class="mt-5">
                        <h1 class="text-lg font-bold tracking-widest uppercase">INQUIRIES</h1>
                    </header>

                    <section class="flex-1 flex items-center">
                        <div class="w-full sm:w-7/12">
                            <p class="w-full text-xs xxl:text-base sm:w-3/4 whitespace-pre-line">
                                {{ $page->pageDescriptions[0]->description }}</p>
                        </div>
                    </section>

                </section>
            </div>
        </div>
        <footer>
            <a href="{{ route('/') }}">
                <img class="w-7" src="{{ asset('img/BACK.png') }}" alt="back btn">
            </a>
        </footer>
    </div>

    <script>
   document.addEventListener('keyup', function(event) {
            if (event.key === 'Escape') {
                window.location.href = '/';
            }
    });
  </script>
@endsection
