
@extends('layouts.guest', ['title' => 'Contact', 'description' => $page->pageDescriptions[0]->description])

@section('content')
    <div class="app min-h-screen flex flex-col overflow-x-hidden">
        <div class="flex-1 w-full h-full swiper-container xxl:mt-20">
            <div class="swiper-wrapper">
                <section class="swiper-slide">
                    <header class="mt-5">
                        <h1 class="text-lg font-bold tracking-widest uppercase">NEWS UPDATE</h1>
                    </header>
                    <section class="flex-1 flex items-center "style ="margin-top:20px">
        <div class="w-full " style=" height: 430px; overflow-y: scroll; position: relative;">
            <div class="w-full  sm:w-7/12 text-xs xxl:text-base whitespace-pre-line">
                <?php
                $description = htmlspecialchars($page->pageDescriptions[0]->description);
                $decodedDescription = html_entity_decode($description);
                ?>
                {!! $decodedDescription !!}
            </div>
        </div>
    </section>

    </div>
        </div>
        <footer>
            <a href="{{ route('/') }}">
                <img class="w-7" src="{{ asset('img/BACK.png') }}" alt="back btn">
            </a>
        </footer>
    </div>
    <script>
    // Mendapatkan referensi tombol kembali ke home
    document.addEventListener('keyup', function(event) {
            if (event.key === 'Escape') {
                window.location.href = '/';
            }
    });
  </script>
@endsection

<style>
  /* Ganti warna latar belakang, lebar, dan tinggi scrollbar sesuai kebutuhan Anda */
  ::-webkit-scrollbar {
    width: 8px;
    height: 8px;
    background-color: #f5f5f5;
  }

  /* Ganti warna latar belakang bagian luar scrollbar sesuai kebutuhan Anda */
  ::-webkit-scrollbar-track {
    background-color: #f5f5f5;
  }

  /* Ganti warna latar belakang bagian dalam scrollbar sesuai kebutuhan Anda */
  ::-webkit-scrollbar-thumb {
    background-color: #888;
    border-radius: 3px;
  }
</style>


<!-- 
                    <section class="flex-1 flex items-center"> -->
                    <!-- <div class="flex flex-wrap mt-4">
                             <div class="w-full lg:w-1/3 self-start relative mr-4">
                        @foreach ($page->images as $image)
                        <img src="{{ asset('storage/'.$image->src) }}" class="w-full">
                        @endforeach
                        {{-- <div class="nested-swiper-container overflow-hidden w-full">
                            <div class="swiper-wrapper">
                                @foreach ($page->images as $image)
                                <div class="swiper-slide">
                                    <img src="{{ asset('storage/'.$image->src) }}" class="w-full">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="z-20 btn-next-nested absolute top-0 right-0 bottom-0 left-0 outline-none"></div>
                        --}}
                    </div>
                    <div class="w-full sm:w-7/12 ml-4">
    <p class="w-full text-xs xxl:text-base mt-0whitespace-pre-line">
        {{ $page->pageDescriptions[0]->description }}
    </p>
</div>

                    </section> -->