@extends('layouts.guest', [
'title' => $page->name,
'description' => $page->pageDescriptions->where('description_type', 'right')->first()->description,
'image' => count($orderedImages) > 0 ? asset('storage/' . $orderedImages[0]->src) : null
])

@section('content')
<div class="app min-h-screen flex flex-col overflow-x-hidden justify-center">
  <div class="swiper-container w-full h-full flex items-center">
    <div class="swiper-wrapper items-center">
      <section class="swiper-slide">
        <div class="flex flex-wrap">
          <div class="w-full md:w-1/4">
            <p class="text-xs xxl:text-base whitespace-pre-line">
              {{ $page->pageDescriptions->where('description_type', 'left')->first()->description }}
            </p>
          </div>
          <div class="w-full md:w-1/2 mt-10 md:mt-0 lg:ml-32">
            <p class="text-xs xxl:text-base whitespace-pre-line">
              {{ $page->pageDescriptions->where('description_type', 'right')->first()->description }}
            </p>
          </div>
        </div>
      </section>
      @foreach ($orderedImages as $image)
      <section class="swiper-slide project-container flex items-center">
        <img src="{{ asset('storage/'.$image->src) }}" class="mx-auto w-auto xl:h-full max-h-full">
      </section>
      @endforeach
    </div>

    <div class="btn-prev fixed">
      <svg class="w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175">
        <path d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225
          c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z" />
      </svg>
    </div>

    <div class="btn-next fixed">
      <svg class="w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175">
        <path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5
          c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z
        " />
      </svg>
    </div>
  </div>

  <footer class="self-start mt-10 text-xs xxl:text-sm w-full">
    <div class="flex items-center mt-3">
      <a href="{{ route('/') }}" class="inline-block">
        <img class="w-7" src="{{ asset('img/BACK.png') }}" alt="back btn">
      </a>
      <div class="flex items-center ml-4">
        <div class="btn-pagination" style="width: auto !important;">
          <div class="swiper-pagination-current"></div>
          <div class="swiper-pagination-total"></div>
        </div>
        <h4 class="ml-3">{{ $page->name }}</h4>
      </div>
    </div>
  </footer>
</div>
@endsection

@push('js')
<script>
  const swiper = new Swiper('.swiper-container', {
    preloadImages: false,
    lazy: true,
    loop: true,
    spaceBetween: 30,
    speed: 750,
    pagination: {
      el: '.btn-pagination',
      type: 'fraction',
      renderFraction: function (currentClass, totalClass) {
        return '<span class="' + currentClass + '"></span>' +
          '/' +
          '<span class="' + totalClass + '"></span>';
      },
    },
    navigation: {
      nextEl: '.btn-next',
      prevEl: '.btn-prev',
    },
    keyboard: {
      enabled: true,
      onlyInViewport: false,
    }
  });

  const prevButton = document.querySelector('.btn-prev');
  const nextButton = document.querySelector('.btn-next');

  prevButton.addEventListener('click', function () {
    swiper.slidePrev();
  });

  nextButton.addEventListener('click', function () {
    swiper.slideNext();
  });

  document.addEventListener('keyup', function(event) {
    if (event.key === 'Escape') {
      window.location.href = '/';
    }
  });
</script>
@endpush
