<section id="slide-2"
    class="slide-2 swiper-slide grid grid-max-h grid-cols-2 sm:grid-cols-3 xl:grid-cols-5 xl:grid-rows-3 gap-3">
    <a href="{{ route('project', $projects[14]) }}" class="group relative overflow-hidden">
        <div class="nested-swiper-container h-full">
            <div class="swiper-wrapper">
                @foreach ($slideTwo->where('name', 'project_one')->first()->images as $image)
                <div class="swiper-slide">
                    <img loading="lazy" src="{{ asset('storage/' . $image->src) }}" class="w-full h-full">
                </div>
                @endforeach
            </div>
        </div>

        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
            {{ $slideTwo->where('name', 'project_one')->first()->content }}
        </div>
    </a>
    <a href="{{ route('project', $projects[15]) }}" class="group relative overflow-hidden">
        <div class="nested-swiper-container h-full">
            <div class="swiper-wrapper">
                @foreach ($slideTwo->where('name', 'project_two')->first()->images as $image)
                <div class="swiper-slide">
                    <img loading="lazy" src="{{ asset('storage/' . $image->src) }}" class="w-full h-full">
                </div>
                @endforeach
            </div>
        </div>

        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
            {{ $slideTwo->where('name', 'project_two')->first()->content }}
        </div>
    </a>
    <a href="{{ route('project', $projects[16]) }}" class="group relative overflow-hidden">
        <div class="nested-swiper-container h-full">
            <div class="swiper-wrapper">
                @foreach ($slideTwo->where('name', 'project_three')->first()->images as $image)
                <div class="swiper-slide">
                    <img loading="lazy" src="{{ asset('storage/' . $image->src) }}" class="w-full h-full">
                </div>
                @endforeach
            </div>
        </div>

        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
            {{ $slideTwo->where('name', 'project_three')->first()->content }}
        </div>
    </a>
    <a href="{{ route('project', $projects[17]) }}" class="group relative overflow-hidden">
        <div class="nested-swiper-container h-full">
            <div class="swiper-wrapper">
                @foreach ($slideTwo->where('name', 'project_four')->first()->images as $image)
                <div class="swiper-slide">
                    <img loading="lazy" src="{{ asset('storage/' . $image->src) }}" class="w-full h-full">
                </div>
                @endforeach
            </div>
        </div>

        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
            {{ $slideTwo->where('name', 'project_four')->first()->content }}
        </div>
    </a>
    <a href="{{ route('project', $projects[18]) }}" class="group relative overflow-hidden">
        <div class="nested-swiper-container h-full">
            <div class="swiper-wrapper">
                @foreach ($slideTwo->where('name', 'project_five')->first()->images as $image)
                <div class="swiper-slide">
                    <img loading="lazy" src="{{ asset('storage/' . $image->src) }}" class="w-full h-full">
                </div>
                @endforeach
            </div>
        </div>

        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
            {{ $slideTwo->where('name', 'project_five')->first()->content }}
        </div>
    </a>
    <a href="{{ route('project', $projects[19]) }}" class="group relative overflow-hidden">
        <div class="nested-swiper-container h-full">
            <div class="swiper-wrapper">
                @foreach ($slideTwo->where('name', 'project_six')->first()->images as $image)
                <div class="swiper-slide">
                    <img loading="lazy" src="{{ asset('storage/' . $image->src) }}" class="w-full h-full">
                </div>
                @endforeach
            </div>
        </div>

        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
            {{ $slideTwo->where('name', 'project_six')->first()->content }}
        </div>
    </a>
    <a href="{{ route('project', $projects[20]) }}" class="group relative overflow-hidden col-span-2 row-span-2">
        <div class="nested-swiper-container h-full">
            <div class="swiper-wrapper">
                @foreach ($slideTwo->where('name', 'project_seven')->first()->images as $image)
                <div class="swiper-slide">
                    <img loading="lazy" src="{{ asset('storage/' . $image->src) }}" class="w-full h-full">
                </div>
                @endforeach
            </div>
        </div>

        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
            {{ $slideTwo->where('name', 'project_seven')->first()->content }}
        </div>
    </a>
    <div class="grid grid-cols-1 grid-rows-2 gap-3">
    <a href="{{ route('about') }}" class="group relative overflow-hidden">
    @if ($about && $about->images->count() > 0)
        <img loading="lazy" src="{{ asset('storage/' . $about->images[0]->src) }}" class="w-full h-full transform transition-transform duration-300 hover:scale-105">
    @endif
    <div style="background: rgba(0, 0, 0, 0.35);"
    class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
        {{ $about ? $about->content : 'about content is empty!' }}
    </div>       
</a>
<a href="{{ route('news') }}" class="group relative overflow-hidden">
    @if ($logo && $logo->images->count() > 0)
        <img loading="lazy" src="{{ asset('storage/' . $logo->images[0]->src) }}" class="w-full h-full transform transition-transform duration-300 hover:scale-105">
    @endif
    <div style="background: rgba(0, 0, 0, 0.35);"
    class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
        {{ $logo ? $logo->content : 'Logo content is empty!' }}
    </div>       
</a>
    </div>
    <div class="grid grid-cols-1 grid-rows-2 gap-3">
    <a href="{{ route('inquiries') }}" class="group relative overflow-hidden">
        @if ($inq && $inq->images->count() > 0)
            <img loading="lazy" src="{{ asset('storage/' . $inq->images[0]->src) }}" class="w-full h-full transform transition-transform duration-300 hover:scale-105">
        @endif
        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform group-hover:mt-0 group-hover:-translate-y-full">
            {{ $inq ? $inq->content : 'Logo content is empty!' }}
        </div>
    </a>
       
    <a href="{{ $setting->site_media_url }}" class="group relative overflow-hidden" target="_blank">
    @if ($media && $media->images->count() > 0)
            <img loading="lazy" src="{{ asset('storage/' . $media->images[0]->src) }}" class="w-full h-full transform transition-transform duration-300 hover:scale-105">
        @endif
        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform group-hover:mt-0 group-hover:-translate-y-full">
            {{ $media ? $media->content : 'Logo content is empty!' }}
        </div>
    </a>  
    </a>
    </div>
    <a href="{{ route('project', $projects[21]) }}" class="group relative overflow-hidden">
        <div class="nested-swiper-container h-full">
            <div class="swiper-wrapper">
                @foreach ($slideTwo->where('name', 'project_eight')->first()->images as $image)
                <div class="swiper-slide">
                    <img loading="lazy" src="{{ asset('storage/' . $image->src) }}" class="w-full h-full">
                </div>
                @endforeach
            </div>
        </div>

        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
            {{ $slideTwo->where('name', 'project_eight')->first()->content }}
        </div>
    </a>
    <a href="{{ route('project', $projects[22]) }}" class="group relative overflow-hidden">
        <div class="nested-swiper-container h-full">
            <div class="swiper-wrapper">
                @foreach ($slideTwo->where('name', 'project_nine')->first()->images as $image)
                <div class="swiper-slide">
                    <img loading="lazy" src="{{ asset('storage/' . $image->src) }}" class="w-full h-full">
                </div>
                @endforeach
            </div>
        </div>

        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
            {{ $slideTwo->where('name', 'project_nine')->first()->content }}
        </div>
    </a>
    <a href="{{ route('project', $projects[23]) }}" class="group relative overflow-hidden">
        <div class="nested-swiper-container h-full">
            <div class="swiper-wrapper">
                @foreach ($slideTwo->where('name', 'project_ten')->first()->images as $image)
                <div class="swiper-slide">
                    <img loading="lazy" src="{{ asset('storage/' . $image->src) }}" class="w-full h-full">
                </div>
                @endforeach
            </div>
        </div>

        <div style="background: rgba(0, 0, 0, 0.35);"
            class="whitespace-pre-line z-20 absolute w-full h-full p-5 xxl:p-8 text-white text-xs sm:text-sm xxl:text-base transition duration-500 transform  group-hover:mt-0 group-hover:-translate-y-full">
            {{ $slideTwo->where('name', 'project_ten')->first()->content }}
        </div>
    </a>
</section>

<style>
    img {
  width: 100%;
  height: 100%;
  object-fit: cover;
}
</style>
