<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>{{ $title . ' | ' . $setting->site_name }}</title>
    <meta name="title" content="{{ $title . ' | ' . $setting->site_name }}">
    <meta name="description" content="{{ $description }}">
    <meta name="keywords" content="Architecture , LAB, Local Architecture Bureau, Tan Tik Lam, Gemawang Swaribathoro">
    <meta name="author" content="LAB | Local Architecture Bureau">

    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:title" content="{{ $title . ' | ' . $setting->site_name }}">
    <meta property="og:description" content="{{ $description }}">
    <meta property="og:image" content="{{ $image ?? asset($setting->site_logo) }}">

    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="{{ url()->current() }}">
    <meta property="twitter:title" content="{{ $title . ' | ' . $setting->site_name }}">
    <meta property="twitter:description" content="{{ $description }}">
    <meta property="twitter:image" content="{{ $image ?? asset($setting->site_logo) }}">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('img/BACK.png') }}">

    <!-- FONTS-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body class="xl:overflow-y-hidden">
    @yield('content')

    <!-- JS -->
    <script src="{{ mix('js/swiper.js') }}"></script>
    @stack('js')
    <script src="{{ mix('js/app.js') }}"></script>
</body>

</html>
