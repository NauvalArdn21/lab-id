<?php

use App\Page;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'title' => 'Home',
                'name' => 'Home',
                'slug' => 'home'
            ],
            [
                'title' => 'about-lab',
                'name' => 'About 1',
                'slug' => 'about-1'
            ],
            [
                'title' => 'partner',
                'name' => 'About 2',
                'slug' => 'about-2'
            ],
            [
                'title' => 'lab-people',
                'name' => 'About 3',
                'slug' => 'about-3'
            ],
            [
                'title' => 'client',
                'name' => 'About 4',
                'slug' => 'about-4'
            ],
            [
                'title' => 'News Update',
                'name' => 'News Update',
                'slug' => 'news-update'
            ],
            [
                'title' => 'Inquiries',
                'name' => 'Inquiries',
                'slug' => 'inquiries'
            ]
        ];

        for ($i = 1; $i <= 40; $i++) {
            Page::create($this->slider($i));
        }

        foreach ($pages as $page) {
            Page::create($page);
        }
    }

    public function slider($id)
    {
        return
            [
                'title' => 'Project ' .  $id,
                'name' => 'Project ' .  $id,
                'slug' => 'project-' .  $id
            ];
    }
}
