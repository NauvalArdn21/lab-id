<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'site_name' => 'LAB',
            'site_description' => 'LAB works on a wide range architecture projects, from medium to large scale with interest to Public, Commercial & Private uses. LAB covers a think-tank process, idea development and research-based study to implementation.',
            'site_logo' => 'img/LOGO.png',
            'site_media_url' => 'https://google.com'
        ]);
    }
}
