!(function (e) {
    var n = {};
    function t(r) {
        if (n[r]) return n[r].exports;
        var o = (n[r] = { i: r, l: !1, exports: {} });
        return e[r].call(o.exports, o, o.exports, t), (o.l = !0), o.exports;
    }
    (t.m = e),
        (t.c = n),
        (t.d = function (e, n, r) {
            t.o(e, n) ||
                Object.defineProperty(e, n, { enumerable: !0, get: r });
        }),
        (t.r = function (e) {
            "undefined" != typeof Symbol &&
                Symbol.toStringTag &&
                Object.defineProperty(e, Symbol.toStringTag, {
                    value: "Module",
                }),
                Object.defineProperty(e, "__esModule", { value: !0 });
        }),
        (t.t = function (e, n) {
            if ((1 & n && (e = t(e)), 8 & n)) return e;
            if (4 & n && "object" == typeof e && e && e.__esModule) return e;
            var r = Object.create(null);
            if (
                (t.r(r),
                Object.defineProperty(r, "default", {
                    enumerable: !0,
                    value: e,
                }),
                2 & n && "string" != typeof e)
            )
                for (var o in e)
                    t.d(
                        r,
                        o,
                        function (n) {
                            return e[n];
                        }.bind(null, o)
                    );
            return r;
        }),
        (t.n = function (e) {
            var n =
                e && e.__esModule
                    ? function () {
                          return e.default;
                      }
                    : function () {
                          return e;
                      };
            return t.d(n, "a", n), n;
        }),
        (t.o = function (e, n) {
            return Object.prototype.hasOwnProperty.call(e, n);
        }),
        (t.p = "/"),
        t((t.s = 0));
})({
    0: function (e, n, t) {
        t("bUC5"), (e.exports = t("Ge+w"));
    },
    "9Wh1": function (e, n) {
        new Swiper(".nested-swiper-container", {
            loop: !0,
            slideToClickedSlide: !0,
            navigation: { nextEl: ".btn-next-nested" },
        });
    },
    "Ge+w": function (e, n) {},
    bUC5: function (e, n, t) {
        function r(e) {
            if ("undefined" == typeof Symbol || null == e[Symbol.iterator]) {
                if (
                    Array.isArray(e) ||
                    (e = (function (e, n) {
                        if (!e) return;
                        if ("string" == typeof e) return o(e, n);
                        var t = Object.prototype.toString.call(e).slice(8, -1);
                        "Object" === t &&
                            e.constructor &&
                            (t = e.constructor.name);
                        if ("Map" === t || "Set" === t) return Array.from(t);
                        if (
                            "Arguments" === t ||
                            /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t)
                        )
                            return o(e, n);
                    })(e))
                ) {
                    var n = 0,
                        t = function () {};
                    return {
                        s: t,
                        n: function () {
                            return n >= e.length
                                ? { done: !0 }
                                : { done: !1, value: e[n++] };
                        },
                        e: function (e) {
                            throw e;
                        },
                        f: t,
                    };
                }
                throw new TypeError(
                    "Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
                );
            }
            var r,
                i,
                u = !0,
                c = !1;
            return {
                s: function () {
                    r = e[Symbol.iterator]();
                },
                n: function () {
                    var e = r.next();
                    return (u = e.done), e;
                },
                e: function (e) {
                    (c = !0), (i = e);
                },
                f: function () {
                    try {
                        u || null == r.return || r.return();
                    } finally {
                        if (c) throw i;
                    }
                },
            };
        }
        function o(e, n) {
            (null == n || n > e.length) && (n = e.length);
            for (var t = 0, r = new Array(n); t < n; t++) r[t] = e[t];
            return r;
        }
        t("9Wh1"),
            document.body.addEventListener("keydown", function (e) {
                "Escape" === e.key && (document.location.href = "/");
            });
        var i = document.querySelectorAll(".slide-1"),
            u = document.querySelectorAll(".slide-2"),
            c = document.querySelectorAll(".slide-3"),
            l = document.querySelector("section.swiper-slide-active"),
            a = document.querySelector(
                "section.swiper-slide-active .col-span-2"
            );
        function f() {
            !(function e() {
                var n = l.getAttribute("id"),
                    t = Array.from(l.children)
                        .map(function (e) {
                            return { sort: Math.random(), value: e };
                        })
                        .sort(function (e, n) {
                            return e.sort - n.sort;
                        })
                        .map(function (e) {
                            return e.value;
                        });
                if ("slide-1" === n)
                    for (var r = 4; r < t.length; r++) {
                        if (t[r] === a) return void e();
                    }
                else if ("slide-2" === n) {
                    for (var o = 9; o < t.length; o++) {
                        if (t[o] === a) return void e();
                    }
                    for (var i = 0; i < 5; i++) {
                        if (t[i] === a) return void e();
                    }
                } else
                    for (var u = 4; u < t.length; u++) {
                        if (t[u] === a) return void e();
                    }
                t.forEach(function (e) {
                    l.appendChild(e);
                }),
                    (l = document.querySelector("section.swiper-slide-active")),
                    (a = document.querySelector(
                        "section.swiper-slide-active .col-span-2"
                    ));
            })();
        }
        function d() {
            v = setInterval(f, randomDuration);
        }
        function s() {
            clearInterval(v);
        }
        var v = setInterval(f, randomDuration);
        i.forEach(function (e) {
            var n,
                t = r(e.children);
            try {
                for (t.s(); !(n = t.n()).done; )
                    (child = n.value),
                        child.addEventListener("mouseenter", function () {
                            s();
                        }),
                        child.addEventListener("mouseleave", function () {
                            d();
                        });
            } catch (e) {
                t.e(e);
            } finally {
                t.f();
            }
        }),
            u.forEach(function (e) {
                var n,
                    t = r(e.children);
                try {
                    for (t.s(); !(n = t.n()).done; )
                        (child = n.value),
                            child.addEventListener("mouseenter", function () {
                                s();
                            }),
                            child.addEventListener("mouseleave", function () {
                                d();
                            });
                } catch (e) {
                    t.e(e);
                } finally {
                    t.f();
                }
            }),
            c.forEach(function (e) {
                var n,
                    t = r(e.children);
                try {
                    for (t.s(); !(n = t.n()).done; )
                        (child = n.value),
                            child.addEventListener("mouseenter", function () {
                                s();
                            }),
                            child.addEventListener("mouseleave", function () {
                                d();
                            });
                } catch (e) {
                    t.e(e);
                } finally {
                    t.f();
                }
            });
    },
});
