module.exports = {
    purge: [
        './resources/views/**/*.blade.php',
        './resources/views/**/**/*.blade.php',
    ],
    theme: {
        extend: {
            width: {
                '7': '1.75rem'
            },
            gridTemplateRows: {
                '8': 'repeat(8, minmax(0, 1fr))',
            },
            margin: {
                '9': '2.4rem',
                '14': '3.5rem'
            },
        },
        screens: {
            'sm': '640px',
            'md': '768px',
            'lg': '1024px',
            'xl': '1280px',
            'xxl': '1680px',
        }
    },
    variants: {
        translate: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
        margin: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
        backgroundColor: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
        opacity: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
    },
    plugins: [],
}
